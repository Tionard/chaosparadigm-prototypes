/// @func CloseToFloor()
/// @description script that checks if an object is touching the ceiling or floor
/// @returns <boolean> on_floor

var on_floor;

if (place_meeting(x , y + sign(vsp), oCollisions))
		{	on_floor = true;	}
else	{	on_floor = false;	}

return on_floor;