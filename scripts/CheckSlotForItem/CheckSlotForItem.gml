/// @func CheckSlotForItem(slot,item)
/// @description allows to compare the equiped slot item with given item name
/// @param {String} slot name
/// @param {String} item name


//It is a placeholder-script for now
var matches;

var slot = argument0;
var item = argument1;

if skeleton_attachment_get(slot) = item 
			{matches = true; }
	else	{matches = false;}

return matches;