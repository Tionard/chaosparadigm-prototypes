///@func AnimationMapsTest()
///@description One time script to create all ds_maps for the player
///@returns anim_list;

anim_list = ds_list_create();
var map_number = 0;

body_map = ds_map_create();
AnimationsToMap(body_map, "body", ANIMATION_TRACKS.BODY);
ds_list_add(anim_list,body_map);
map_number++;

hands_map = ds_map_create();
//AnimationsToMap(hands_map, "hands", ANIMATION_TRACKS.HANDS);
AnimationsToMap(hands_map, "hands_gun", ANIMATION_TRACKS.HANDS);
ds_list_add(anim_list,hands_map);
map_number++;

head_map = ds_map_create();
AnimationsToMap(head_map, "head", ANIMATION_TRACKS.HEAD);
ds_list_add(anim_list,head_map);
map_number++;

legs_map = ds_map_create();
AnimationsToMap(legs_map, "legs", ANIMATION_TRACKS.LEGS);
ds_list_add(anim_list,legs_map);
map_number++;

for (var i = 0; i < map_number; ++i) {
	ds_list_mark_as_map(anim_list,i)
	}
	
return anim_list;