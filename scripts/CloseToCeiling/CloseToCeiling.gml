/// @func CloseToCeiling()
/// @description script that checks if an object is touching the ceiling or floor
/// @returns <boolean> on_ground

var bellow_ceiling;

bellow_ceiling = false;

if(main_state = STATES.SIT || main_state = STATES.STEALTH)
	if (place_meeting(x , y - 5, oCollisions))
			{	bellow_ceiling = true;	}
	else	{	bellow_ceiling = false;	}

return bellow_ceiling;