///@desc it changes animation of all body_parts to specific key animation
///@param KEY to serch inside ds_maps

var KEY = argument0;

#region Set Idle Animation
for (var i = 0; i < ds_list_size(anim_list); ++i) 
{
	var _map =	anim_list[| i];
	var _key =	_map[? KEY];
	var _track = _map[? "TRACK"];
	skeleton_animation_set_ext(_key ,_track);
}

#endregion
