/// @func SpeedControl()
/// @desc It is separate script tp conrol speed of the Player

if(CloseToGround()) //change movement speed only if on the ground
{
	if	( key_shift && !key_sit && (IsFacing()>=0) && !CloseToCeiling() )	
						{	hspd = MOV_SPD.RUN;		}
	else if	(key_sit)||(CloseToCeiling())	{	hspd = MOV_SPD.STEALTH;	}
	else				{	hspd = MOV_SPD.WALK;	}	


//allowes to change direction or make hsp=0;
//pull outside of "if(CloseToGround())" brackets to allow control while in air
h_move = sign(key_right - key_left); 
}									

hsp	 = hspd * h_move;




#region Adcanced Jump

//Set Up jump delay. It allowes player to jump during short time after he left ground
//It's not the same as double jump!
if ( CloseToGround() ) 
{	
	canJump = JUMP_DELAY;	
}

if	(canJump>0)&&(!CloseToCeiling())
{
	if(key_jump)&&(!key_sit)
	{
		vsp = -MOV_SPD.JUMP * sign(key_jump);	//Jump if possible
		canJump = 0;							//Set the jump delay to 0 - 										//it disables ability to jump again while in air
	}
	else if(key_jump)
	{
		vsp = -MOV_SPD.JUMP+3 * sign(key_jump);	//Jump if possible
		canJump = 0;							//Set the jump delay to 0 - 	
	}
	canJump--;		//reduce jump delay until it reaches 0.
}

//the longer you're holding the jump button - the higher you'll jump
if	((vsp<0) && (!key_jump_held))
{
	vsp = max(vsp,-MOV_SPD.JUMP/3);
}

//apply gravity, to pool the oHero down at all times
vsp += grav;

#endregion
