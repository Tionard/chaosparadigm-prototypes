///@func RangeWeaponAnimation()

#region Local Variables

						 //move to macros
var	outer_angle   = 180, //max posible angle while facing any direction
	maximal_angle = 124, //max allowed angle for aim facing any direction
	minimal_angle = 55;  //min allowed angle for aim facing any direction
	
	// allowed diapason of angles in any direction
	//var allowed_diapason = max(0,min(aim_direction - minimal_angle,69)); 
	var an_name = skeleton_animation_get_ext(1);
	var max_frames = skeleton_animation_get_frames(string(an_name));
	var allowed_range = clamp(aim_direction - minimal_angle,0,max_frames-1);	
	
#endregion

#region Hands aim_direction

skeleton_animation_set_frame(1,allowed_range);

#endregion

#region Hands Recoil Animation

//hard coded:
/*
if(mouse_check_button_pressed(mb_left))
{
	skeleton_animation_set_ext("2hands_recoil",1);
	alarm[0] = 8;
}
*/


#endregion