///@func AnimationMix(key1,key2,speed);
///@desc mixes KEY1 animation of each part(body,hands,legs,head) into KEY2 animations with given SPEED
///@param KEY1 to serch inside ds_maps
///@param KEY2 to serch inside ds_maps
///@param SPEED between 0 and 1. how fast would the animation change

var KEY1 = argument0;
var KEY2 = argument1;
var SPEED = argument2;

#region MIX 2 sets of animations

for (var i = 0; i < ds_list_size(anim_list); ++i) 
{
	//if (i != 1) //**(not useful right now)** mix all animation for all parts except for hands
	{
		var _map =	anim_list[| i];
		var _key1 =	_map[? KEY1];
		var _key2 =	_map[? KEY2];
		skeleton_animation_mix(_key1 ,_key2, SPEED);
	}
}

#endregion
