/// @func isCloseGround()
/// @description script that checks if an object is 1 frame away from the ground
/// @returns <boolean> ground

var ground;

if (place_meeting(x , y + vsp, oCollisions))
		{	ground = true;	}
else	{	ground = false;	}

return ground;