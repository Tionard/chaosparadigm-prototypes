/// @func IsFacing()
/// @desc what direction the object is currently faces(image_xscale) relative to his horizontal speed(hsp)
/// @returns {Real} faces
/// returns 0	if the object doesn't move horizontally (not used now)
/// returns 1	if the object faces the same direction as it moves horizontally
/// returns -1	if the object faces opposite direction relative to its movement

		// if I only need 1 and -1 as return
if(hsp != 0)
	faces = sign(image_xscale * hsp);
else
	faces = sign(image_xscale);


//faces = sign(image_xscale * hsp);

return faces;