///@description Controls for player

//hspd = MOV_SPD.WALK;

key_right	  = keyboard_check(ord("D"));
key_left	  = keyboard_check(ord("A"));
key_up		  = keyboard_check(ord("W"));
key_down	  = keyboard_check(ord("S"));
key_jump	  = keyboard_check_pressed(vk_space);

if keyboard_check_pressed(vk_backspace) 
{debug = !debug;}

key_jump_held = keyboard_check(vk_space);
key_shift	  = keyboard_check(vk_shift);

key_interract = keyboard_check_pressed(ord("E"));

key_ctrl	  =	keyboard_check(vk_control);
key_fire	  = mouse_check_button(mb_left);
key_start	  = keyboard_check_pressed(vk_escape);

key_sit = (key_ctrl || key_down);

