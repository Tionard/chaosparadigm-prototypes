/// @func CheckForWalls()
/// @description script that checks if an object is touching the wall or not
/// @returns <boolean> wall

var wall;

if (place_meeting(x + hsp, y ,oCollisions))
		{	wall = true;	}
else	{	wall = false;}

return wall;