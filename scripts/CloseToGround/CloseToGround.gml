/// @func CloseToGround()
/// @description script that checks if an object is touching the ceiling or floor
/// @returns <boolean> on_ground

var on_ground;

if (place_meeting(x , y + 1, oCollisions))
		{	on_ground = true;	}
else	{	on_ground = false;	}

return on_ground;