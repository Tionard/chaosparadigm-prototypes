enum STATES
{
	IDLE,
	WALK,
	RUN,
	SIT,
	STEALTH, 
	JUMP,
	FALL,
	LADDER,
	DEATH,
	DIALOG
}

enum HANDS_STATES
{
	EMPTY,
	GUN,
	SWORD,
	BIG_SWORD
}

enum ANIMATION_TRACKS
{
	BODY  = 0,
	HANDS = 1,
	HEAD  = 2,
	LEGS  = 3
}

enum MOV_SPD
{
	IDLE = 0,
	STEALTH = 1,
	WALK = 3, //was 2 before
	RUN = 6,
	JUMP = 12
}