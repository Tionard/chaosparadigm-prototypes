/// @func GotStuck()
/// @description script that checks if player is inside of any oCollision obj
/// @returns <boolean> stuck

var stuck;

if (place_meeting(x , y, oCollisions))
		{	stuck = true;	}
else	{	stuck = false;	}

return stuck;