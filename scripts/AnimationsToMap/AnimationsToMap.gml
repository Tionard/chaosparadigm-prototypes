/// @func AnimationsToMap()
/// @description Script writes a set of animations into choosen ds_map
/// @param {ds_map} map_name is a name of ds_map to save to
/// @param {String} body_part is a string that would be used as a part of animation_name
/// @param {Integer} track is a track to play the animation on

var map_name = argument0;
var body_part = argument1;
var animation_track = argument2;

map_name[? "TRACK"] = animation_track;
map_name[? "IDLE"]	= body_part + "_idle";
map_name[? "WALK"]	= body_part + "_walk";
map_name[? "RUN"]	= body_part + "_run";
map_name[? "SIT"]	= body_part + "_sit";
map_name[? "STEALTH"]	= body_part + "_stealth";
map_name[? "JUMP"]	= body_part + "_jump";
map_name[? "FALL"]	= body_part + "_fall";