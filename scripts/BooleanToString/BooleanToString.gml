/// @func BooleanToString(boolean)
/// @desc just converts 1 and 0 to true or false, in other case returns "unknown"

var answer

if (argument0 = 1)		{	answer = "true";	}
else if (argument0 = 0)	{	answer = "false";	}
else					{	answer = "unknown";	}

return answer;