///@func Movement()

if (CheckForWalls())
{
	while(!CloseToWall())
	{
		x += sign(hsp);
	}
	hsp = 0;
}
x += hsp;	


if (CheckForFloors())
{
	while(!CloseToFloor())
	{
		y += sign(vsp);
	}
	vsp = 0;
}
y += floor(vsp);

