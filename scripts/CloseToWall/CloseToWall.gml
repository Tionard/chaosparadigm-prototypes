/// @func CloseToWall()
/// @description script that checks if an object is touching the wall or not
/// @returns <boolean> on_wall
var on_wall;

if (place_meeting(x + 1*sign(image_xscale)*IsFacing(), y ,oCollisions))
		{	on_wall = true;	}
else	{	on_wall = false;}

return on_wall;