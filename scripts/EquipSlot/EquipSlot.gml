/// @func Equip(slot,item)
/// @description allows to equip an item(arg0) in selected slot(arg1)
/// @param {String} slot name
/// @param {String} item name


//It is a placeholder-script for now

var slot = argument0;
var item = argument1;

skeleton_attachment_set(slot, item);