/// @func GetSlot(slot)
/// @description allows to equip an item(arg0) in selected slot(arg1)
/// @param {String} slot name


//It is a placeholder-script for now

var slot = argument0;

return skeleton_attachment_get(slot);