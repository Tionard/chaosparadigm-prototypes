{
    "id": "309e6ed4-83e3-43f1-940f-91ac8b1e953c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLight",
    "eventList": [
        {
            "id": "bd949c39-8f2c-49db-ae86-c27a5c0e5264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "309e6ed4-83e3-43f1-940f-91ac8b1e953c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7ab85828-b6f9-4d3d-901b-77c59af58608",
    "visible": true
}