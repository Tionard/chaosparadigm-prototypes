{
    "id": "ac97dd69-3a70-4b87-b9cd-3162720042b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCollisions",
    "eventList": [
        {
            "id": "3ba31542-e39e-4c2f-a9ac-081ddb39105e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac97dd69-3a70-4b87-b9cd-3162720042b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "659c1ed8-6be9-4c9b-9a9a-5d0762ca1f13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6ed50a5e-d7cd-4fbd-928e-b35531cd65d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "75df5d8a-6801-4448-a761-947b5df9d551",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "163a6559-471c-488d-a256-404d18796e1a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "10e93e04-f400-4a37-9d5b-852b15831c4a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "2cc0cf31-13bc-40ce-a0ae-1c2eab9ae6b3",
    "visible": false
}