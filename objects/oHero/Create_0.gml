/// @description initialization


//assign standart "idle" collision mask
Equip("col_mask","standing");

//assign "noone" without ", to create inner light from inside of step event code
inner_light = oInnerLight;


#region <boolean> variables
						// if true:
mstate_changed = false;	// main state has changed
hstate_changed = false; // hands state has changed (equipped/unequipped weapon)
sit_lock = false;
debug = false;

#endregion

#region <real> variables

h_move = 0;			//sum of movement buttons pressed
aim_direction = 0;	// aim_direction
hsp		= 0;		// moving speed
vsp		= 0;		// jumping acceleration
grav	= 0.5;		// base gravity speed
canJump = 0;		//timer for jump

hspd = MOV_SPD.WALK;
faces = sign(image_xscale * hspd);

#endregion

#region other variables

main_state	= STATES.IDLE;
hands_state = HANDS_STATES.EMPTY;

#endregion

#region buffer variables

//<real> variables
hsp_buf		= 0;	//buffer for horizontal speed
vsp_buf		= 0;	//buffer for vertical	speed

//<state> variables
mstate_buf	= main_state;	//buffer for main state of character
hstate_buf	= hands_state;	//buffer for hands state of character

#endregion

#region initialize and set animations

/*
Script below creates a list of maps with set of animations for every single part of body
such as head, hands, legs, body. Each part assigns to its own animation track so 
they can play independent from each other.
*/
AnimationMaps();

/*
Script below runs through list of animation maps created inside AnimationMaps() script.
It takes String argument such as "IDLE" or "RUN" etc. to assign correct animation to 
each single body part(head, body, hands, legs);
*/
SetAllAnimations("IDLE");

#endregion

#region animations mix


AnimationMix("JUMP",	"FALL",		0.4);
AnimationMix("JUMP",	"SIT",		0.2);

AnimationMix("RUN",		"JUMP",		0.3);
AnimationMix("RUN",		"FALL",		0.3);
AnimationMix("RUN",		"WALK",		0.15);
AnimationMix("RUN",		"IDLE",		0.1);
AnimationMix("RUN",		"SIT",		0.25);
AnimationMix("RUN",		"STEALTH",	0.25);


AnimationMix("WALK",	"JUMP",		0.3);
AnimationMix("WALK",	"FALL",		0.3);
AnimationMix("WALK",	"IDLE",		0.05);
AnimationMix("WALK",	"RUN",		0.15);
AnimationMix("WALK",	"SIT",		0.25);
AnimationMix("WALK",	"STEALTH",	0.25);

AnimationMix("STEALTH", "WALK",		0.1);
AnimationMix("STEALTH", "RUN",		0.1);
AnimationMix("STEALTH", "JUMP",		0.2);
AnimationMix("STEALTH", "FALL",		0.25);
AnimationMix("STEALTH", "SIT",		0.15);
AnimationMix("STEALTH", "IDLE",		0.15);

AnimationMix("IDLE",	"JUMP",		0.2);
AnimationMix("IDLE",	"FALL",		0.25);
AnimationMix("IDLE",	"WALK",		0.05);
AnimationMix("IDLE",	"RUN",		0.1);
AnimationMix("IDLE",	"SIT",		0.25);
AnimationMix("IDLE",	"STEALTH",	0.25);

AnimationMix("SIT",		"JUMP",		0.2);
AnimationMix("SIT",		"FALL",		0.25);
AnimationMix("SIT",		"WALK",		0.25);
AnimationMix("SIT",		"RUN",		0.25);
AnimationMix("SIT",		"IDLE",		0.25);
AnimationMix("SIT",		"STEALTH",	0.25);

AnimationMix("FALL",	"WALK"	,	0.1);
AnimationMix("FALL",	"RUN"	,	0.2);
AnimationMix("FALL",	"IDLE"	,	0.1);
AnimationMix("FALL",	"SIT"	,	0.2);
AnimationMix("FALL",	"STEALTH",	0.2);

#endregion

weapon0 = skeleton_attachment_create("empty1",spr_box,0,0,0,1,1,0);

//skeleton_attachment_set("SlotRangeWeapon","PlasmaGun");