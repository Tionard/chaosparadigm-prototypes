/// @description Controlls, Checks and State-Machine


//create the InnerLight for the character and move it with him
if(!instance_exists(inner_light))
{
	inner_light = instance_create_layer(x,y,"Light",oInnerLight);
}
else
{
	with(inner_light)
	{
		x = other.x;
		y = other.y - other.sprite_height/2;
		light[| eLight.X] = other.x;
		light[| eLight.Y] = other.y - other.sprite_height/2;
	}
}

#region this is only variables needed for hands with a gun

var xk,yk;	//move to enum if it stays as konstant
xk = 12;	//x offset of the shoulder relative to the root(origin) point
yk = 90;	//y offset of the shoulder relative to the root(origin) point

hand_x = x-xk*image_xscale;
hand_y = y-yk*image_yscale;

mdir_x = hand_x+(mouse_y-hand_y);
mdir_y = hand_y-(mouse_x-hand_x)*sign(image_xscale);

aim_direction = point_direction(hand_x,hand_y,mdir_x,mdir_y);

#endregion

KeyboardControls();

//CollisionBoxControl();
SpeedControl();
//PlayerStateChange();
Checkers();

Movement();

ImageSpeedControl();


#region test movement area

if aim_direction > 180
{
	image_xscale = -image_xscale;
}
#endregion

#region change main state

	switch (main_state) 
	{
	    case STATES.IDLE:	 PlayerIdle();		break;
		case STATES.WALK:	 PlayerWalk();		break;
		case STATES.RUN:	 PlayerRun();		break;
		case STATES.SIT:	 PlayerSit();		break;
		case STATES.STEALTH: PlayerStealth();	break;
		case STATES.JUMP:	 PlayerInAir();		break;
		case STATES.FALL:	 PlayerInAir();		break;
		case STATES.DEATH:	 PlayerDeath();		break;
		
	    default: show_message("oHero Step Event: Invalid State")	break;
	}

#endregion