/// @description 

// destroy data structures
if ds_exists(anim_list, ds_type_list)
{
	ds_list_destroy(anim_list);
}
else
{
	show_message("list anim_list doesn't exists!");
}
