/// @description debub info

draw_self();

if(debug)
{
	real_rotation = point_direction(hand_x,hand_y,mouse_x,mouse_y)

	var a = camera_get_view_x(view_camera[0]);
	var b = camera_get_view_y(view_camera[0]);

	draw_set_color(c_white);
	draw_text(a+100,b+80, "real angle:    "+string(real_rotation));
	draw_text(a+100,b+100,"virtual angle: "+string(aim_direction));
	draw_text(a+100,b+120,"frames total:  "+string(skeleton_animation_get_frames(string(skeleton_animation_get_ext(1)))));
	draw_text(a+100,b+140,"current frame: "+string(skeleton_animation_get_frame(1)));
	draw_set_color(c_white);
	draw_text(a+300,b+80, "track0: "+string(skeleton_animation_get_ext(0)));
	draw_text(a+300,b+100,"track1: "+string(skeleton_animation_get_ext(1)));
	draw_text(a+300,b+120,"track2: "+string(skeleton_animation_get_ext(2)));
	draw_text(a+300,b+140,"track3: "+string(skeleton_animation_get_ext(3)));
	draw_set_color(c_green);
	draw_text(a+100,b+180, "ground: "+string(	CheckForFloors()	));
	draw_text(a+100,b+200, "wall:	"+string(	CheckForWalls()	));
	draw_text(a+100,b+220, "vsp:	"+string(vsp));
	draw_text(a+100,b+240, "hsp:	"+string(hsp));
	draw_text(a+300,b+180, "on ground:	"+string( BooleanToString(CloseToGround()) ));
	draw_text(a+300,b+200, "on wall:	"+string(	BooleanToString(CloseToWall())	));
	draw_text(a+300,b+220, "faces:		"+string(	IsFacing()	));
	draw_text(a+300,b+240, "jump:		"+string(	canJump	));
	draw_text(a+100,b+260, "check item:	"+string(CheckSlotForItem("col_mask","sitting")));

	skeleton_collision_draw_set(true);

	draw_set_color(c_red);
	draw_circle(mdir_x,mdir_y,4,false);
	draw_circle(hand_x,hand_y,1,false);
	draw_set_color(c_green);
	draw_circle(mouse_x,mouse_y,6,false);
	draw_line(hand_x,hand_y,mouse_x,mouse_y);

	draw_set_color(c_blue);
	//pivot point testing
	if aim_direction>=55 and aim_direction <= 125
	{
		draw_circle(hand_x+image_xscale*15*sin(degtorad(real_rotation)),hand_y+image_xscale*15*cos(degtorad(real_rotation)),8,false);
		draw_circle(mouse_x+image_xscale*15*sin(degtorad(real_rotation)),mouse_y+image_xscale*15*cos(degtorad(real_rotation)),8,false);

		draw_circle(hand_x+image_xscale*15*sin(degtorad(real_rotation))+image_xscale*50*sin(degtorad(aim_direction)),
		hand_y+image_xscale*15*cos(degtorad(real_rotation))+abs(image_xscale)*50*cos(degtorad(aim_direction)),8,false);
	}
	else if aim_direction>125 and aim_direction < 180
	{
		draw_circle(hand_x+image_xscale*15*sin(degtorad(35)),hand_y+abs(image_xscale)*15*cos(degtorad(35)),8,false);

		draw_circle(hand_x+image_xscale*15*sin(degtorad(35))+image_xscale*300*sin(degtorad(125)),
		hand_y+abs(image_xscale)*15*cos(degtorad(35))+abs(image_xscale)*300*cos(degtorad(125)),8,false);

		draw_circle(hand_x+image_xscale*15*sin(degtorad(35))+image_xscale*50*sin(degtorad(125)),
		hand_y+abs(image_xscale)*15*cos(degtorad(35))+abs(image_xscale)*50*cos(degtorad(125)),8,false);
	}
	else if  aim_direction < 55 and aim_direction >= 0
	{
		draw_circle(hand_x+image_xscale*15*sin(degtorad(325)),hand_y+abs(image_xscale)*15*cos(degtorad(325)),8,false);

		draw_circle(hand_x+image_xscale*15*dsin(325)+image_xscale*300*dsin(55),
		hand_y+abs(image_xscale)*15*dcos(325)+abs(image_xscale)*300*dcos(55),8,false);

		draw_circle(hand_x+image_xscale*15*dsin(325)+image_xscale*50*dsin(55),
		hand_y+abs(image_xscale)*15*dcos(325)+abs(image_xscale)*50*dcos(55),8,false);
	}
	
}
else
{
	skeleton_collision_draw_set(false);
}