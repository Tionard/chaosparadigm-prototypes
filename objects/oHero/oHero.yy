{
    "id": "3bc8dfc3-f729-4313-940b-f47bc2fa783d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHero",
    "eventList": [
        {
            "id": "afde54f8-1c5f-4ca4-b34b-5a46d7d92041",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        },
        {
            "id": "ffa652ee-b89b-49a1-a476-894ce3ca4f99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        },
        {
            "id": "3d72bb0e-972c-4bb7-8470-d2f48cd30cef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        },
        {
            "id": "1a49791d-8d60-43de-a5ad-b1c097ac8d5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        },
        {
            "id": "70f398c4-e6fc-4ef2-a138-211b6c642045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        },
        {
            "id": "9ca4fc98-a66f-4bfe-b54d-dda31716dc46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3bc8dfc3-f729-4313-940b-f47bc2fa783d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0c0fb82a-b155-4669-af60-61c19d159068",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "grav",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "085543bd-4dc9-4d46-a91d-b35d23f32770",
    "visible": true
}