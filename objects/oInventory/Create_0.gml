/// @description 
#region create items map. Better to place inside create event of a "controler"-type object later

items_map = ds_map_create();

armors_map = ds_map_create();
ds_map_add_map(items_map,"ARMOR",armors_map);

weapons_map = ds_map_create();
ds_map_add_map(items_map,"WEAPON",weapons_map);

potions_map = ds_map_create();
ds_map_add_map(items_map,"POTION",potions_map);

misc_map = ds_map_create();
ds_map_add_map(items_map,"MISC",misc_map);


#endregion