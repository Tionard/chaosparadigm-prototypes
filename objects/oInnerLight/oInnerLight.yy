{
    "id": "dccc65d0-4249-4d95-bef3-34c547dddac0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInnerLight",
    "eventList": [
        {
            "id": "318e821c-10fe-4bbe-a141-ef5b1eb3e17c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dccc65d0-4249-4d95-bef3-34c547dddac0"
        },
        {
            "id": "bc6220e0-7ddf-4e56-9d3e-f779442f31ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dccc65d0-4249-4d95-bef3-34c547dddac0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df52e9b8-9ae8-4f9d-8f74-8a00f1c9b4c1",
    "visible": false
}