/// @description 

event_inherited();

light[| eLight.Intensity] = 0.75;
light[| eLight.Range] = 300;
light[| eLight.Color] = $77BFEBFF;
