/// @description Insert description here



// keyboard controlls
restart_btn = keyboard_check_pressed(vk_numpad0)
//funktions are responsible for diferent room aspects
next_funktion = keyboard_check_pressed(vk_add);			
prev_funktion = keyboard_check_pressed(vk_subtract);

incr_btn = keyboard_check_pressed(vk_multiply);	
decr_btn = keyboard_check_pressed(vk_divide);	

//restart room
if (restart_btn) {room_restart();}

funktion = funktion + next_funktion - prev_funktion;
funktion = clamp(funktion,-1,funktions_number);

switch(funktion)
{
	case 0: 
	//set the funktion name
	funk_name = "eLight.Intensity";
		
		if(instance_exists(oLight))
		with(oLight)
		{
			if (light[| eLight.Intensity] < other.ONE*3)
				light[| eLight.Intensity] += other.incr_btn*0.1;
				
			if (light[| eLight.Intensity] > other.ZERO)
				light[| eLight.Intensity] -= other.decr_btn*0.1;
				
			other.value = string(light[| eLight.Intensity]);
		}
		
	break;
	
	
	case 1:
	//set the funktion name
	funk_name = "ambientShadowIntensity";
		
		global.ambientShadowIntensity = clamp(global.ambientShadowIntensity + incr_btn*0.01 - decr_btn*0.01, ZERO, ONE)
		value = global.ambientShadowIntensity;
	break;
	
	
	case 2:
	//set the funktion name
	funk_name = "oCamera.size_multiplier";
	
		with(oCamera)
		{
			if (other.decr_btn && (size_multiplier > 0.5))
			{
				size_multiplier -= 0.05
				camera_set_view_size(cam , standart_width *size_multiplier, standart_height *size_multiplier);
			}
			else if(other.incr_btn && (size_multiplier < 1.5))
			{
				size_multiplier += 0.05
				camera_set_view_size(cam , standart_width *size_multiplier , standart_height *size_multiplier );
			}
			
			//update variables
			view_w_half = camera_get_view_width(cam)  * 0.5;
			view_h_half = camera_get_view_height(cam) * 0.5;
			
			//update camera position
			camera_set_view_pos(cam,x-view_w_half,y-view_h_half);
			
			//assign value
			other.value = size_multiplier;
		}
		
	break;
	
	default:
		funk_name = "none";
		value = "none";
	break;
}