/// @description Set Up Camera

cam = view_camera[0];
if instance_exists(oHero)	
	follow = oHero;
else
{
	follow = self;
	show_message("oHero"+string(oHero)+" doesn't exists");
}
size_multiplier = 1;

standart_width = camera_get_view_width(cam);
standart_height = camera_get_view_height(cam);

view_w_half = camera_get_view_width(cam)  * 0.5;
view_h_half = camera_get_view_height(cam) * 0.5;
xTo = xstart;
yTo = ystart;
