{
    "id": "9ee2b287-e478-4972-9a65-06bced26a7d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "fe7b0cb7-d578-46ae-971d-8b1e50db8fd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ee2b287-e478-4972-9a65-06bced26a7d5"
        },
        {
            "id": "d1ee5972-4c5e-4bf7-8e4b-f6c1ccf9e48d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9ee2b287-e478-4972-9a65-06bced26a7d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "32ab9cfa-4f12-4206-9d11-76873d10e29a",
    "visible": false
}