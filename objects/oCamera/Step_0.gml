/// @description Update Camera

#region local variables
	
	var min_asx = 100,	//min_asx - minimal allowed shift x
		max_asx = 100,	//max_asx - maximal allowed shift x
		min_asy = 315,	//min_asy - minimal allowed shift y
		max_asy = 65;	//max_asy - maximal allowed shift y
		
	var x_spd_div = 20,
		y_spd_div = 20;
		
#endregion
//Update Destination
if (instance_exists(follow))
{
	
	xTo = clamp(mouse_x, follow.x-min_asx, follow.x+max_asx);
	
	yTo = follow.y - max_asy;
	
/* //not usefull at the moment
	if(!keyboard_check(vk_control))
		yTo = clamp(mouse_y-max_asy*5, follow.y-min_asy, follow.y-max_asy);
	else
		yTo = clamp(mouse_y+max_asy*5, follow.y-min_asy, follow.y-max_asy);
*/	
}

//Update Camera Position
x += (xTo - x) / x_spd_div;
y += (yTo - y) / y_spd_div;

x = clamp(x, view_w_half, room_width  - view_w_half);
y = clamp(y, view_h_half, room_height - view_h_half);

//Update Camera View
camera_set_view_pos(cam,x-view_w_half,y-view_h_half);



