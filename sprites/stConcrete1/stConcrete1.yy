{
    "id": "e5ec3c10-6bb0-4ebd-9a63-11c9389126a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "stConcrete1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 130,
    "bbox_left": 0,
    "bbox_right": 394,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15a76d76-f0e2-4bea-b52a-5593a9d266cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5ec3c10-6bb0-4ebd-9a63-11c9389126a1",
            "compositeImage": {
                "id": "60d0e684-cde9-4dbf-9ab1-63114e2e72ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a76d76-f0e2-4bea-b52a-5593a9d266cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da60ee26-f9a3-4150-9b33-120d80d9d6ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a76d76-f0e2-4bea-b52a-5593a9d266cd",
                    "LayerId": "648a337a-2821-4a6c-a37c-eb225c1835fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "648a337a-2821-4a6c-a37c-eb225c1835fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5ec3c10-6bb0-4ebd-9a63-11c9389126a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 395,
    "xorig": 0,
    "yorig": 0
}