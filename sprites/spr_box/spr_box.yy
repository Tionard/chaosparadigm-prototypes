{
    "id": "aea319e9-7879-46f8-a465-8efceb040057",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32a50d81-1e00-4d77-980a-956e0364d015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aea319e9-7879-46f8-a465-8efceb040057",
            "compositeImage": {
                "id": "e3c4a7e2-e6b7-44b0-ad15-e260205d7063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a50d81-1e00-4d77-980a-956e0364d015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae4c252-e967-4769-95a0-b268f67432a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a50d81-1e00-4d77-980a-956e0364d015",
                    "LayerId": "6caf693c-78cc-4496-acae-c493b4e88b02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6caf693c-78cc-4496-acae-c493b4e88b02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aea319e9-7879-46f8-a465-8efceb040057",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 30,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}