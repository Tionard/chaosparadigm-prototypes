{
    "id": "085543bd-4dc9-4d46-a91d-b35d23f32770",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHero",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 130,
    "bbox_left": 0,
    "bbox_right": 103,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e7dd6df-dcd3-41e6-a3d2-1deff5efe1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "085543bd-4dc9-4d46-a91d-b35d23f32770",
            "compositeImage": {
                "id": "cf568350-381f-42e3-aa05-ef15cb3240e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7dd6df-dcd3-41e6-a3d2-1deff5efe1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc0fee0-dba8-41cf-b7f3-4e13ccbf73ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7dd6df-dcd3-41e6-a3d2-1deff5efe1c5",
                    "LayerId": "6162e6e2-55f5-45af-9fc7-d338fffb1a18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "6162e6e2-55f5-45af-9fc7-d338fffb1a18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "085543bd-4dc9-4d46-a91d-b35d23f32770",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 2,
    "width": 104,
    "xorig": 51,
    "yorig": 130
}