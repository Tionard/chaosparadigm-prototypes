{
    "id": "f02f25d7-9fae-4d5c-b372-8d671ddafb54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 130,
    "bbox_left": 0,
    "bbox_right": 103,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10900490-b135-4f1d-af66-c997dd435ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f02f25d7-9fae-4d5c-b372-8d671ddafb54",
            "compositeImage": {
                "id": "a3d2d805-7f4a-4861-976c-a4eb50c63fbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10900490-b135-4f1d-af66-c997dd435ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494e0376-6cb0-41aa-9536-96629523927c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10900490-b135-4f1d-af66-c997dd435ffe",
                    "LayerId": "d470a397-40bd-4406-9a8d-3385690487f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "d470a397-40bd-4406-9a8d-3385690487f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f02f25d7-9fae-4d5c-b372-8d671ddafb54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 2,
    "width": 104,
    "xorig": 51,
    "yorig": 130
}