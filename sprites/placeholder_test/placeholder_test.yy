{
    "id": "864d8320-7cdc-45be-844f-cbaeb0322da7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "placeholder_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43c4fc16-50b8-4dc3-afdc-24a0059b1db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864d8320-7cdc-45be-844f-cbaeb0322da7",
            "compositeImage": {
                "id": "205231f9-99f0-4f6f-8379-649c0f3b7cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c4fc16-50b8-4dc3-afdc-24a0059b1db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493e6a2c-ea7d-4172-9e6d-91814682891e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c4fc16-50b8-4dc3-afdc-24a0059b1db5",
                    "LayerId": "f73d5522-8361-42e0-8def-8db6050da875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f73d5522-8361-42e0-8def-8db6050da875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "864d8320-7cdc-45be-844f-cbaeb0322da7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}