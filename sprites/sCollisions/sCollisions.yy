{
    "id": "2cc0cf31-13bc-40ce-a0ae-1c2eab9ae6b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCollisions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ff9516d-73be-4520-b757-6ae847730791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cc0cf31-13bc-40ce-a0ae-1c2eab9ae6b3",
            "compositeImage": {
                "id": "ce9d2a34-a982-41c6-8c7f-c727c3a02276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff9516d-73be-4520-b757-6ae847730791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09ad6ee-e566-4214-bad5-ee3b39c2dd5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff9516d-73be-4520-b757-6ae847730791",
                    "LayerId": "195d02ff-6dcf-498a-93ee-c0c066d77e37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "195d02ff-6dcf-498a-93ee-c0c066d77e37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cc0cf31-13bc-40ce-a0ae-1c2eab9ae6b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 40,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}