{
    "id": "32ab9cfa-4f12-4206-9d11-76873d10e29a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCamera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bae023d-7d9a-4ec1-94e5-7eebc0c06b74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ab9cfa-4f12-4206-9d11-76873d10e29a",
            "compositeImage": {
                "id": "b17a8091-3375-4fd8-860c-bffc77f00ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bae023d-7d9a-4ec1-94e5-7eebc0c06b74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337cf0c5-7766-49e9-bf9a-d24bfed32887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bae023d-7d9a-4ec1-94e5-7eebc0c06b74",
                    "LayerId": "b5837f6a-20cb-482f-aeaa-07768b92c007"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b5837f6a-20cb-482f-aeaa-07768b92c007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32ab9cfa-4f12-4206-9d11-76873d10e29a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}