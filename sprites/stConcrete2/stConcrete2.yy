{
    "id": "fe4e9b7a-461e-44ef-9001-a874b13c0153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "stConcrete2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 130,
    "bbox_left": 0,
    "bbox_right": 394,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a389e5a6-b902-4520-88de-32e66f1454e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe4e9b7a-461e-44ef-9001-a874b13c0153",
            "compositeImage": {
                "id": "248eb6de-1708-43d4-b92a-7d6a046a1a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a389e5a6-b902-4520-88de-32e66f1454e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f0e89c-cf9b-47ec-86d6-1aee3ff20145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a389e5a6-b902-4520-88de-32e66f1454e8",
                    "LayerId": "3d9c4aba-cbeb-4c8c-a970-59662bd28047"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "3d9c4aba-cbeb-4c8c-a970-59662bd28047",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe4e9b7a-461e-44ef-9001-a874b13c0153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 395,
    "xorig": 0,
    "yorig": 0
}