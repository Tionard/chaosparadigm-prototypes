{
    "id": "d7f99558-9b48-4889-8ea1-31c0c5a0a86c",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tConcrete",
    "auto_tile_sets": [
        {
            "id": "d67f2c0e-d098-4f33-91c3-339c3978c30c",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                43,
                39,
                42,
                34,
                41,
                37,
                35,
                31,
                40,
                33,
                38,
                30,
                36,
                29,
                32,
                28,
                18,
                17,
                16,
                12,
                21,
                20,
                19,
                13,
                24,
                23,
                22,
                14,
                27,
                25,
                26,
                15,
                45,
                46,
                9,
                5,
                10,
                6,
                11,
                7,
                8,
                4,
                2,
                1,
                47,
                3,
                44
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 15,
        "SerialiseWidth": 14,
        "TileSerialiseData": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            46,
            3,
            0,
            1,
            3,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            9,
            21,
            10,
            0,
            9,
            10,
            0,
            2,
            0,
            0,
            0,
            0,
            0,
            0,
            8,
            27,
            11,
            0,
            8,
            11,
            0,
            47,
            0,
            0,
            0,
            0,
            2,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            45,
            0,
            9,
            21,
            10,
            0,
            9,
            10,
            0,
            2,
            0,
            0,
            0,
            0,
            45,
            0,
            18,
            43,
            24,
            0,
            18,
            24,
            0,
            45,
            0,
            0,
            0,
            0,
            47,
            0,
            8,
            27,
            11,
            0,
            8,
            11,
            0,
            47,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            46,
            46,
            3,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ]
    },
    "out_columns": 7,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "e5ec3c10-6bb0-4ebd-9a63-11c9389126a1",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 48,
    "tileheight": 32,
    "tilehsep": 1,
    "tilevsep": 1,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}